# pipscript

Simple python script(s) geared towards custom installation of 
3rd party python packages using various features of pip.

Specifically started to address features missing from pip, 
such as those referenced here:

 - https://stackoverflow.com/questions/33441033/pip-install-to-custom-target-directory-and-exclude-specific-dependencies
 - https://github.com/pypa/pip/issues/3090?_pjax=%23js-repo-pjax-container
 
## Examples

### pipcustominstall.py
 Install a list of packages from text file (example provided) into custom directory 
 without redundant packages already in current python environment

```
% git clone git@gitlab.com:bscipio/pipscript.git
% cd pipscript
% ./bin/pipcustominstall.py ./examples/extra_pkgs.txt ./custompkgs
```
 
 