#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""

Workaround script written in response to:

https://stackoverflow.com/questions/33441033/pip-install-to-custom-target-directory-and-exclude-specific-dependencies
https://github.com/pypa/pip/issues/3090?_pjax=%23js-repo-pjax-container

Created on Wed Oct 28 19:37:38 2015
@author: Brian Staab
"""


import pip

import re,sys,os,shutil,argparse,glob



def main(epfn,pkg_install_dir,builddir=None, cleanup=False):
    """
    Install python packages to custom directory without redundancies in current environment
    
    epfn : Requirements file of python packages to install. See pip help install -r option for formatting
    pkg_install_dir : Directory to install packages', type=str)    
    
    Note: This script does not take versions into account when determining redundancies    
    
    """
    

    if builddir == None:
        builddir = os.path.curdir
        
    builddir = os.path.abspath(builddir)

    if not os.path.exists(pkg_install_dir):
        raise Exception('Specified install folder: %s does not exist.'%(pkg_install_dir))

    if not os.path.exists(builddir):
        raise Exception('Specified build folder: %s does not exist.'%(builddir))

        
    print("Package Build folder: %s"%builddir)
    print("Package Install folder: %s"%pkg_install_dir)
    
    pipdldir = os.path.join(builddir,"pip_src")
    
    
    # read the list of packages from file
    with open(epfn,"r") as f:
        epl = [line.rstrip() for line in f]
    
    # make an extra packages dictionary
    ep_dict = {}
    for p in epl:
        if "=" in p:
            ps = p.split("=")
            ep_dict[ps[0]] = ps[-1]
        else: # No version specified, store None
            ep_dict[p] = None
    
    # get currently installed packages
    ipkg = pip.get_installed_distributions()
    ipkg_list = sorted(["%s==%s" % (i.key, i.version) for i in ipkg])
    
    ipkg_dict  = {i.key: i.version for i in ipkg}
    
    print("#"*60)
    print("Checking already installed packages vs. requested extras")
    
    # Check if already installed, and print warning
    print("Requested Extra Packages        Existing Packages" )
    for ep,epv in ep_dict.iteritems():
        if epv == None: epv = ""
        
        if ep in ipkg_dict.keys():
            print("{:>20s} {:10} {} {}".format(ep,epv, ep,ipkg_dict[ep]))     
            #print("Requested Extra Package:     {} {}".format(ep,epv))        
            #print("****Existing Package***:     {} {}\n".format(ep,ipkg_dict[ep]))         
        else:
            #print("Requested Extra Package:     {} {}\n".format(ep,epv)) 
            print("{:>20s} {:10}".format(ep,epv)) 
            
    print("#"*60)
    
    
    # download the extra packages in target directory
    # including potential redundant packages
    pipdlog = os.path.join(builddir,"pip_downloads.log")
    
    pipargs = ["download","--no-cache-dir","--log",pipdlog, "-r", epfn,"--dest", pipdldir]

    
    dl = pip.main(pipargs)
    if dl != 0:
        raise ValueError("non-zero pip status. pip failed to download all packages")

    #
    # There must bet a better way than log parsing
    #
    dstr = "Successfully downloaded"

    with open(pipdlog,"r") as f:
        
        for line in f:
            if dstr in line:
                dlist = line.replace(dstr,"").split()
                
    print("Downloaded Packages:")
    print(dlist)
                
    # create some sets
    dset = set(dlist) # downloaded set
    
    iset = set(ipkg_dict.keys()) # already installed set
                
                
    # The resulting set has elements of the downloaded set with all elements 
    # from the already-installed set removed. 
    # An element will be in the result (fset) if it is in the downloaded set (dset) 
    # and not in the installed set(iset).
    #
    # Note: This does not take package versions into account when determining redundancies  
    fset = dset - iset          

    # Determine packages that were not downloaded
    print("DID NOT DOWNLOAD LIST: Packages that pip did not download")
    print(iset - dset)


    if len(fset) == 0:
        raise ValueError("No packages to install. All packages may already be in the python config currently in use")
                
    
    print("FILTERED LIST: Packages to install. No duplicates from current env")
    print(fset)            
                
                
    # write a new requirements based on fset
    
    installfn = os.path.join(builddir, "pypkgs_and_deps_noDuplicates.txt")
    with open(installfn,"w") as f:
        f.write('\n'.join(fset))
    
    
    # install the extra packages in target directory 
    # from installfn (fset)
    
    pipilog = os.path.join(builddir,"pip_install.log")
    
    # "--no-index & --find-links used since the packages should already be downloaded into builddir
    # no need to download again
    pipargs = ["install","--no-index","--find-links=%s"%(pipdldir),"--no-cache-dir","--no-deps","--no-compile","--log",pipilog, "-r", installfn,"-t", pkg_install_dir]
    
    
    i = pip.main(pipargs)

    if i != 0:
        raise ValueError("non-zero pip status. pip failed to install some packages")    
    
    # move eggs and dist-info to build dir for a cleaner install area
    eggdir = os.path.join(builddir, "pip_egg-info")
    if not os.path.exists(os.path.join(eggdir)):
        os.makedirs(eggdir)    
    
    for eggfn in glob.glob(os.path.join(pkg_install_dir,"*.egg-info")):
        shutil.move(eggfn, eggdir)

    for distfn in glob.glob(os.path.join(pkg_install_dir,"*dist-info")):
        shutil.move(distfn, eggdir)
    
    
    # test import new packages
    #    
    #  https://stackoverflow.com/questions/13598035/importing-a-module-when-the-module-name-is-in-a-variable
    #  https://stackoverflow.com/questions/1057431/loading-all-modules-in-a-folder-in-python
    #
    # NOTE: If .pyc files are not desired do not perform the following, or remove them after test/install    
    sys.path.insert(1, pkg_install_dir)
    
    import importlib
    import pkgutil
    
    for importer, package_name, _ in pkgutil.iter_modules([pkg_install_dir]):
        print 'Importing %s from %s' % (package_name, pkg_install_dir)
    
        try:
            module = importlib.import_module(package_name, package=None)   
        except:
            print("WARNING: {} failed to import".format(package_name)) 
    
    
    # Cleanup
    #
    # remove builddir
    if cleanup:    
        shutil.rmtree(builddir)


if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='Install python packages to custom directory without duplicating ones present in current python environment.', 
                                     epilog="NOTE: Redundancies are not checked for specific package versions")
    parser.add_argument('packagelistfile', help='Requirements file of python packages to install. See pip -r help for formatting', type=str)
    parser.add_argument('installdir',      help='Directory to install packages', type=str)
    parser.add_argument('-b','--builddir', help='Directory to download and build packages. Store .egg files. Default = Current directory', type=str)

    if len(sys.argv[1:])<2:
        parser.print_help()

        # parser.print_usage() # for just the usage line
        parser.exit()
    

#    try:
#        args = parser.parse_args()
#    except:
#        parser.print_help()
#        sys.exit(0)

    args = parser.parse_args()
    
    main(args.packagelistfile,
         args.installdir,
         args.builddir)
        
    
        